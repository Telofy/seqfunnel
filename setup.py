#!/usr/bin/env python
from setuptools import setup
setup(
    name='seqfunnel',
    version='0.0.1',
    author='Telofy',
    author_email='seqfunnel@yu-shin.de',
    include_package_data=True,
    extras_require=dict(
        test=[],
    ),
    install_requires=[
        'setuptools',
    ],
    zip_safe=False,
    entry_points={
        'console_scripts': []
    }
)
