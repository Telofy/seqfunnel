# -*- encoding: utf-8 -*-
from __future__ import unicode_literals, division, \
                       absolute_import, print_function
from Queue import Queue

class Funnel(object):

    def __init__(self):
        self.queue = Queue()

    def __iter__(self):
        while True:
            yield self.queue.get()

    def single(self, func):
        def wrapper(*args, **kwargs):
            value = func(*args, **kwargs)
            self.queue.put(value)
            return value
        return wrapper

    def sequence(self, func):
        def wrapper(*args, **kwargs):
            values = func(*args, **kwargs)
            for value in values:
                self.queue.put(value)
            return values
        return wrapper
