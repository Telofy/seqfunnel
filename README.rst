=========
Seqfunnel
=========

Purpose
-------

You could just rewrite all your functions to take a queue as an extra argument
and put the return values into it. This little thingamajig wraps this logic in
two decorators and one iterator.

Example and Doctest
-------------------

Test using ``bin/python -m doctest README.rst``.

    >>> from random import choice, sample
    >>> from seqfunnel import Funnel
    
    >>> PONIES = ['Twilight Sparkle', 'Fluttershy', 'Pinkie Pie',
    ...           'Rainbow Dash', 'Applejack', 'Rarity']
    
    >>> funnel = Funnel()
    
    >>> @funnel.single
    ... def return_pony(ponies, index):
    ...     return ponies[index]

    >>> @funnel.sequence
    ... def return_ponies(ponies, count):
    ...     return ponies[:count]

    >>> return_pony(PONIES, 0)
    'Twilight Sparkle'
    >>> return_pony(PONIES, 3)
    'Rainbow Dash'
    >>> return_ponies(PONIES, 2)
    ['Twilight Sparkle', 'Fluttershy']
    >>> return_pony(PONIES, 5)
    'Rarity'
    >>> return_ponies(PONIES, 4)
    ['Twilight Sparkle', 'Fluttershy', 'Pinkie Pie', 'Rainbow Dash']

    >>> for pony in funnel:
    ...     print pony
    ...     if funnel.queue.empty():
    ...         break
    Twilight Sparkle
    Rainbow Dash
    Twilight Sparkle
    Fluttershy
    Rarity
    Twilight Sparkle
    Fluttershy
    Pinkie Pie
    Rainbow Dash
